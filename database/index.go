package database

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

const (
	db       = "mysql"
	host     = "127.0.0.1"
	port     = "3306"
	user     = "root"
	password = "123456"
	protocol = "tcp"
	dbname   = "trabalho_golang"
)

const dsn = user + ":" + password + "@" + protocol + "(" + host + ":" + port + ")" + "/" + dbname

func Connect() *sql.DB {
	connect, err := sql.Open("mysql", dsn)

	if err != nil {
		panic(err)
	}

	return connect
}
