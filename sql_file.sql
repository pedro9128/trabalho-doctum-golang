DROP DATABASE IF EXISTS trabalho_golang;
CREATE DATABASE trabalho_golang;

USE trabalho_golang;

DROP TABLE IF EXISTS tasks;
CREATE TABLE `tasks` (
    `id` INT unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    `description` TINYTEXT,
    PRIMARY KEY(`id`)
)