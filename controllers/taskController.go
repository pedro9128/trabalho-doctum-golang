package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/pedro9128/trabalho-doctum-golang/database"
	"gitlab.com/pedro9128/trabalho-doctum-golang/models"
)

func Welcome(response http.ResponseWriter, request *http.Request) {
	fmt.Fprint(response, "Welcome")
}

func Index(response http.ResponseWriter, request *http.Request) {
	conn := database.Connect()
	defer conn.Close()

	var tasks []models.Task

	query, err := conn.Query("SELECT * FROM tasks ORDER BY id DESC")

	if err != nil {
		panic(err.Error())
	}

	for query.Next() {
		var task models.Task

		err = query.Scan(&task.ID, &task.Name, &task.Description)
		if err != nil {
			panic(err.Error())
		}

		tasks = append(tasks, task)
	}

	encoder := json.NewEncoder(response)
	encoder.Encode(tasks)

}

func Show(response http.ResponseWriter, request *http.Request) {
	conn := database.Connect()
	defer conn.Close()
	var task models.Task

	vars := mux.Vars(request)
	id := vars["taskID"]

	err := conn.QueryRow("SELECT * from tasks WHERE id="+id).Scan(&task.ID, &task.Name, &task.Description)

	if err != nil {
		errorHandler(response, request, http.StatusNotFound)
		return
	}
	encoder := json.NewEncoder(response)
	encoder.Encode(task)

}

func Store(response http.ResponseWriter, request *http.Request) {
	response.WriteHeader(http.StatusCreated)
	conn := database.Connect()
	defer conn.Close()

	var newTask models.Task

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err.Error())
	}

	json.Unmarshal(body, &newTask)
	action, err := conn.Prepare("INSERT INTO tasks (name, description) values(?,?)")
	if err != nil {
		panic(err.Error())
	}
	action.Exec(newTask.Name, newTask.Description)
}

func Update(response http.ResponseWriter, request *http.Request) {
	conn := database.Connect()
	defer conn.Close()
	var newTask models.Task

	vars := mux.Vars(request)
	id := vars["taskID"]

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err.Error())
	}

	json.Unmarshal(body, &newTask)

	action, err := conn.Prepare("UPDATE tasks SET name=?, description=? WHERE id=?")
	if err != nil {
		panic(err.Error())
	}
	action.Exec(newTask.Name, newTask.Description, id)

}

func Destroy(response http.ResponseWriter, request *http.Request) {
	conn := database.Connect()
	defer conn.Close()

	vars := mux.Vars(request)
	id := vars["taskID"]

	conn.QueryRow("DELETE FROM tasks WHERE id=" + id)

	fmt.Fprint(response, 200)

}

func errorHandler(response http.ResponseWriter, request *http.Request, status int) {
	response.WriteHeader(status)
	if status == http.StatusNotFound {
		fmt.Fprint(response, "404. Not found")
	}
}
