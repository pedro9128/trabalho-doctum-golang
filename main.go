package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/pedro9128/trabalho-doctum-golang/routes"
)

func main() {
	var router *mux.Router

	log.Printf("Server listening on http://localhost:4000")

	router = mux.NewRouter()

	routes.Routes(router)

	err := http.ListenAndServe(":4000", router)
	if err != nil {
		fmt.Println("Error", err)
	}

}
