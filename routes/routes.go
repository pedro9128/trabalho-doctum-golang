package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/pedro9128/trabalho-doctum-golang/controllers"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/", controllers.Welcome)
	router.HandleFunc("/tasks", controllers.Index).Methods("GET")
	router.HandleFunc("/tasks/{taskID}", controllers.Show).Methods("GET")
	router.HandleFunc("/tasks", controllers.Store).Methods("POST")
	router.HandleFunc("/tasks/{taskID}", controllers.Update).Methods("PUT")
	router.HandleFunc("/tasks/{taskID}", controllers.Destroy).Methods("DELETE")
}
